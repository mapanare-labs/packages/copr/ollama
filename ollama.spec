%define debug_package %{nil}

Name:           ollama
Version:        0.5.1
Release:        1%{?dist}
Summary:        Get up and running with large language models, locally.

License:        MIT
URL:            https://ollama.ai/
Source0:        https://github.com/jmorganca/%{name}/archive/v%{version}.tar.gz
Source1:        https://github.com/jmorganca/%{name}/releases/download/v%{version}/%{name}-linux-amd64

%description
Get up and running with large language models, locally.

%prep
%setup -q

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 0755 %{SOURCE1} $RPM_BUILD_ROOT%{_bindir}/%{name}

%files
%license LICENSE
%doc README.md
%{_bindir}/%{name}

%changelog
* Mon Dec 09 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Mon Jun 10 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Tue May 28 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Wed Apr 10 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Sun Mar 03 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to v0.1.27

* Thu Jan 12 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to v0.1.20

* Thu Dec 28 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to v0.1.17

* Mon Dec 18 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to v0.1.16

* Thu Dec 14 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to v0.1.15

* Fri Dec 01 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to v0.1.13

* Tue Nov 28 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to v0.1.12

* Mon Nov 27 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to v0.1.11

* Fri Nov 17 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to v0.1.10

* Tue Nov 14 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Inital RPM
